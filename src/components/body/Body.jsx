import { Component } from "react";
import BodyInput from "./body-input/BodyInput";
import BodyOutput from "./body-output/BodyOutput";

class Body extends Component {
    render() {
        return (
            <>
                <BodyInput />
                <BodyOutput />
            </>
        )
    }
}

export default Body;