import { Component } from "react";

import likeImage from "../../../assets/images/like.png";

class BodyOutput extends Component {
    render() {
        return (
            <>
                <div className="row mt-3 text-primary">
                    <p>Thông điệp của bạn ở đây</p>
                </div>
                <div className="row mt-3 mb-5">
                    <img alt="like" src={likeImage} style={{width: "100px", margin: "auto"}}/>
                </div>
            </>
        )
    }
}

export default BodyOutput;